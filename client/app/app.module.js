(function () {
    "use strict";
    angular.module("MyApp", ["ui.router", "http-auth-interceptor"]);
})();