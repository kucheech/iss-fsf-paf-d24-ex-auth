module.exports = function (sequelize, Sequelize) {
    const Accounts = sequelize.define("accounts",
        {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            email: {
                type: Sequelize.STRING,
                allowNull: false
            },
            password: {
                type: Sequelize.STRING,
                allowNull: false
            }
        }, {
            tableName: "accounts",
            timestamps: false   // don't add timestamps attributes updatedAt and createdAt            
        });

    return Accounts;
};