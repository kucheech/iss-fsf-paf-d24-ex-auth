(function () {
    "use strict";
    angular.module("MyApp").config(MyConfig);
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function MyConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("main", {
                url: "/main",
                templateUrl: "/views/main.html",
                controller: "MainCtrl",
                controllerAs: "ctrl"
            })
            .state("login", {
                url: "/login",
                templateUrl: "/views/login.html",
                controller: "LoginCtrl",
                controllerAs: "ctrl"
            })
            .state("protected", {
                url: "/protected",
                templateUrl: "/views/protected.html",
                controller: "ProtectedCtrl",
                controllerAs: "ctrl"
            })
            .state("accounts", {
                url: "/accounts",
                templateUrl: "/views/accounts.html",
                controller: "AccountsCtrl",
                controllerAs: "ctrl"
            })
            .state("edit", {
                url: "/edit/:id",
                templateUrl: "/views/edit.html",
                controller: "EditCtrl",
                controllerAs: "ctrl"
            })
            .state("register", {
                url: "/register",
                templateUrl: "/views/register.html",
                controller: "RegisterCtrl",
                controllerAs: "ctrl"
            })
            .state("resetpassword", {
                url: "/resetpassword",
                templateUrl: "/views/resetpassword.html",
                controller: "ResetPasswordCtrl",
                controllerAs: "ctrl"
            })

        $urlRouterProvider.otherwise("/main");
    }





})();