(function () {
    "use strict";
    angular.module("MyApp").controller("ResetPasswordCtrl", ResetPasswordCtrl);

    ResetPasswordCtrl.$inject = ["MyAppService", "$state", "$timeout"];

    function ResetPasswordCtrl(MyAppService, $state, $timeout) {
        var vm = this; // vm
        vm.email = "";      
        vm.status = false;
        vm.message = "";
        vm.showMessage = false;                      

        // vm.init = function () {
        // }

        // vm.init();

        vm.cancel = function() {
            $state.go("login");
        }

        vm.resetPassword = function() {
            vm.showMessage = false;
            
            MyAppService.resetPassword(vm.email)
            .then(function (result) {
                // console.log(result);
                // $state.go("accounts");
                if(result.status == 200) {
                    vm.status = true;
                    vm.message = result.data;
                    vm.showMessage = true;

                    $timeout(function () {
                        $state.go("login");
                    }, 2000);
                } 
            }).catch(function (err) {
                console.log(err);                
                vm.status = false;
                vm.message = err.data;
                vm.showMessage = true;
            });
        }

    }

})();