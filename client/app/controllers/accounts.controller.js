(function () {
    "use strict";
    angular.module("MyApp").controller("AccountsCtrl", AccountsCtrl);

    AccountsCtrl.$inject = ["MyAppService", "$state"];

    function AccountsCtrl(MyAppService, $state) {
        var vm = this; // vm
        vm.accounts = [];

        vm.editAccount = function (id) {
            // console.log(id);
            $state.go("edit", { id: id });
        }

        vm.init = function () {
            MyAppService.getAccounts()
                .then(function (result) {
                    // console.log(result);
                    vm.accounts = result;
                }).catch(function (err) {
                    console.log(err);
                });
        }

        vm.init();


    }

})();