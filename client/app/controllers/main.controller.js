(function () {
    "use strict";

    angular
        .module("MyApp")
        .controller("MainCtrl", MainCtrl);

    MainCtrl.$inject = ["$scope", "$state", "authService"];
    function MainCtrl($scope, $state, authService) {
        var vm = this;

        vm.access = function() {
            $state.go("protected");
        }

        $scope.$on("event:auth-loginRequired", function () {
            console.log("401");
            $state.go("login");
        });

        $scope.$on("event:auth-loginConfirmed", function () {
            console.log("202");
            $state.go("protected");
        });

        $scope.$on("event:auth-forbidden", function () {
            console.log("403");
        });
    }

})();