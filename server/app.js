require('dotenv').load();
// console.log(process.env);

const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");

const session = require("express-session");
const watch = require("connect-ensure-login");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const bCrypt = require("bcrypt-nodejs");


const authenticate = function (username, password, done) {
    // console.log(username + ":" + password);
    // const valid = false;
    Accounts.findOne({
        where: {
            email: username            
        }
    }).then(found => {
        if (found && isCorrectPassword(password, found.password)) {
            return done(null, username);
        } else {
            return done(null, false);
        }
    });

}

passport.use(new LocalStrategy({
    usernameField: "email",
    passwordField: "password"
}, authenticate));

passport.serializeUser(function (username, done) {
    done(null, username);
});

passport.deserializeUser(function (id, done) {
    var userObject = {
        email: id
    }
    done(null, userObject);
});

//settings are parked at .env
// const mailgun = require('mailgun-js')({ apiKey: api_key, domain: DOMAIN });
const mailgun = require('mailgun-js')({ apiKey: process.env.MG_API_KEY, domain: process.env.MG_DOMAIN });

// var data = {
//   from: 'Excited User <me@samples.mailgun.org>',
//   to: 'kucheech@gmail.com',
//   subject: 'Hello',
//   text: 'Testing some Mailgun awesomness!'
// };

// mailgun.messages().send(data, function (error, body) {
//   console.log(body);
// });


//start sequelize
//settings are parked at .env
const Sequelize = require("sequelize");
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD,
    {
        host: process.env.DB_HOST,
        dialect: 'mysql',
        logging: console.log,
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        define: {
            timestamps: false
        }
    }
);
const Accounts = require("./models/accounts")(sequelize, Sequelize);
Accounts.sequelize.sync(function (result) {
    console.log(result);
}).catch(function (err) {
    console.log(err);
});

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({
    secret: "iss-fsf",
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

const BOWER_FOLDER = path.join(__dirname, "/../client/bower_components/");
app.use("/libs", express.static(BOWER_FOLDER));

app.use("/protected", watch.ensureLoggedIn("/status/401"));

app.post("/login", passport.authenticate("local", {
    successRedirect: "/status/202",
    failureRedirect: "/status/403"
}));

app.get("/logout", function (req, res) {
    req.logout();
    req.session.destroy();
    res.status(200).end();
});

app.get("/status/:code", function (req, res) {
    const code = parseInt(req.params.code);
    res.sendStatus(code);
});

app.get("/protected/accounts", function (req, res) {
    Accounts.findAll().then(accounts => {
        res.status(200).json(accounts);
    })
});


//add account
app.post("/resetpassword", function (req, res) {
    const email = req.body.email;

    Accounts.findOne({ where: { email: email } })
        .then(found => {
            if (found) {
                var data = {
                    from: "donotreply@wkupteltd.com",
                    to: email,
                    subject: "Reset password",
                    text: "Hi " + found.name + ",\n\n\nWe have received a request to reset your password.\n\n\nIf you did not make this request or believe this was received in error, please ignore this message."
                };

                mailgun.messages().send(data, function (error, body) {
                    if (error) {
                        console.log(error);
                        console.log(body);
                    } else {
                        console.log(body);
                        res.status(200).send("Reset password email has been sent to " + email);
                    }
                });
            } else {
                res.status(400).send("User does not exist in database");
            }
        }).catch(err => {
            handleError(err, res);
        });

});



app.get("/accounts", function (req, res) {
    Accounts.findAll().then(accounts => {
        res.status(200).json(accounts);
    })
});

app.get("/accounts/:id", function (req, res) {
    const id = req.params.id;
    Accounts.findById(id).then(account => {
        res.status(200).json(account);
    }).catch(err => {
        handleError(err, res);
    });
});

//add account
app.post("/accounts", function (req, res) {
    const account = req.body.account;
    account.password = generateHash(account.password);
    console.log(account);
    Accounts.findOrCreate({ where: { email: account.email }, defaults: account })
        .spread((a, created) => {
            if (created) {
                var data = {
                    from: "donotreply@wkupteltd.com",
                    to: account.email,
                    subject: "Account created",
                    text: "Hi " + account.name + ",\n\n\nWe have received a request to create an account.\n\n\nIf you did not make this request or believe this was received in error, please ignore this message."
                };

                mailgun.messages().send(data, function (error, body) {
                    if (error) {
                        console.log(error);
                        console.log(body);
                        res.status(201).send("Account added to database");
                    } else {
                        console.log(body);
                        res.status(201).send("Account added to database and email sent to " + a.email);
                    }
                });
            } else {
                res.status(422).send("Email already exists in database");
            }
        }).catch(err => {
            handleError(err, res);
        });

});


//update account
app.put("/accounts/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    const account = req.body.account;

    Accounts.update(account, { where: { id: id } }).then(result => {
        res.status(200).send(result);
    }).catch(err => {
        console.log(err);
        res.status(500).send(err);
    });
});

//delete a particular account based on id
app.delete("/accounts/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    // console.log(id);
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    Accounts.destroy({ where: { id: id } }).then(result => {
        res.status(200).end();
    }).catch(function (err) {
        console.log(err);
        res.status(500).send(err);
    });
});


var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'iss-fsf'
});


// //using q
const mkQuery = function (sql, pool) {

    const sqlQuery = function () {
        const defer = q.defer();

        var sqlParams = [];
        for (i in arguments) {
            sqlParams.push(arguments[i]);
        }

        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }

            conn.query(sql, sqlParams, function (err, result) {
                if (err) {
                    defer.reject(err);
                    console.log(err);
                } else {
                    // console.log(result);
                    defer.resolve(result);
                }
                conn.release();
            });
        });

        return defer.promise;
    }

    return sqlQuery;
};

const SELECT_ALL_USERS = "select * from users";
const SELECT_USER_BY_ID = "select * from users where regid = ? limit 1";
const UPDATE_USER_BY_ID = "update users set name = ?, email = ?, phone = ?, dob = ? where regid = ?";


const getAllUsers = mkQuery(SELECT_ALL_USERS, pool);
const getUserById = mkQuery(SELECT_USER_BY_ID, pool);
const updateUserById = mkQuery(UPDATE_USER_BY_ID, pool);

function formatDate(date1) {
    return date1.getFullYear() + '-' +
        (date1.getMonth() < 9 ? '0' : "") + (date1.getMonth() + 1) + '-' +
        (date1.getDate() < 10 ? '0' : "") + date1.getDate();
}

const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});


app.post("/user/:id", function (req, res) {
    const id = req.params.id;
    const user = req.body.user;
    updateUserById(user.name, user.email, user.phone, formatDate(new Date(user.dob)), id)
        .then(function (result) {
            console.log(result);
            // res.status(404).type("text/plain").send("Customer not found");
            res.status(200).end();
        }).catch(function (err) {
            handleError(err, res);
        });
});



app.get("/users", function (req, res) {
    getAllUsers().then(function (result) {
        if (result.length > 0) {
            res.status(200).json(result);
        } else {
            res.status(404).send("No users");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

app.get("/user/:id", function (req, res) {
    const id = req.params.id;
    getUserById(id).then(function (result) {
        if (result.length > 0) {
            var book = result[0];
            res.status(200).json(book);
        } else {
            res.status(404).send("User not found");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

//catch all
app.use(function (req, res) {
    console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
    res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

// console.log(generateHash("1111"));
// console.log(generateHash("1234"));

// const v1 = "1234";
// const v2 = generateHash("1234");
// console.log(isCorrectPassword(v1,v2));

function generateHash(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
};

function isCorrectPassword(userPassword, dbPassword) {
    return bCrypt.compareSync(userPassword, dbPassword);
}

//make the app public. In this case, make it available for the testing platform
module.exports = app