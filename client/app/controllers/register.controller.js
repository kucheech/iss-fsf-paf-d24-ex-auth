(function () {
    "use strict";
    angular.module("MyApp").controller("RegisterCtrl", RegisterCtrl);

    RegisterCtrl.$inject = ["MyAppService", "$state", "$timeout"];

    function RegisterCtrl(MyAppService, $state, $timeout) {
        var vm = this; // vm
        vm.account = {
            name: "",
            email: "",
            password: "",
            password2: ""
        };
        vm.status = false;
        vm.message = "";
        vm.showMessage = false;                              

        // vm.init = function () {
        // }

        // vm.init();

        vm.cancel = function() {
            $state.go("login");
        }

        vm.register = function() {
            vm.showMessage = false;

            MyAppService.createAccount(vm.account)
            .then(function (result) {
                console.log(result);
                if(result.status == 201) {
                    vm.status = true;
                    vm.message = result.data;
                    vm.showMessage = true;

                    $timeout(function () {
                        $state.go("login");
                    }, 3000);
                }
                // $state.go("login");
            }).catch(function (err) {
                console.log(err);
                vm.status = false;
                vm.message = err.data;
                vm.showMessage = true;
            });
        }

    }

})();