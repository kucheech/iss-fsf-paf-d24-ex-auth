(function () {
    angular.module("MyApp").service("MyAppService", MyAppService);

    MyAppService.$inject = ["$http", "$q", "$httpParamSerializerJQLike"];

    function MyAppService($http, $q, $httpParamSerializerJQLike) {
        var service = this;

        //expose the following services
        service.getAccounts = getAccounts;
        service.getAccountById = getAccountById;
        service.updateAccount = updateAccount;
        service.login = login;
        service.logout = logout;
        service.getProtectedAccounts = getProtectedAccounts;
        service.resetPassword = resetPassword;
        service.createAccount = createAccount;
        
        function resetPassword(email) {
            var defer = $q.defer();
            $http.post("/resetpassword", { email: email })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getProtectedAccounts() {
            var defer = $q.defer();

            $http.get("/protected/accounts").then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function logout() {
            var defer = $q.defer();

            $http.get("/logout").then(function (result) {
                console.log(result);
                defer.resolve(result);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function login(user) {
            // console.log("service login: " + user);
            var defer = $q.defer();
            $http.post("/login", user)
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }


        function createAccount(account) {
            var defer = $q.defer();

            $http.post("/accounts", { account: account })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }


        function updateAccount(account) {
            var defer = $q.defer();
            const id = user.id;
            $http.post("/accounts/" + id, { account: account })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getAccountById(id) {
            var defer = $q.defer();

            $http.get("/accounts/" + id).then(function (result) {
                // console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function getAccounts() {
            var defer = $q.defer();

            $http.get("/accounts").then(function (result) {
                console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

    }

})();